# Installation Guide

- Clone the repo `git clone git@bitbucket.org:rosemalejohn/larateam.git`
- Run `composer install && npm install && bower install`
- Compile javascript and css files by running `gulp` or `gulp --production`
- Run app `vagrant up --provision`