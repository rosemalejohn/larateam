import http from './../services/http'

export default {

	get(id) {
		return http.get('projects/' + id);
	},

	store(project) {
		return http.post('projects', project);
	},

	update(project, cb = null) {
		return http.put('projects/' + project.id, project);
	}

}