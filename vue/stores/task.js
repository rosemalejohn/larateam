import http from './../services/http'

export default {

	store(request) {
		return http.post('tasks', request);
	},

	delete(task) {
		return http.delete('tasks/' + task.id);
	} 

}