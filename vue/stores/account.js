import http from './../services/http'

export default {

    getAuthenticatedUser() {
        return http.get('auth')
    }

}
