import http from './../services/http'

export default {

	all() {
		return http.get('teams');
	},

    store(team) {
        return http.post('teams', team);
    },

    update(team) {
        return http.put('teams/' + team.id, team);
    },

    get(id) {
    	return http.get('teams/' + id);
    }
}