export default {
    state: {
        members: [],
        teams: [],
        projects: [],
        categories: []
    },

    init(cb = null) { 
        http.get('data', {}, data => {
            this.state.members = data.members;
            this.state.teams = data.teams;
            this.state.projects = data.projects;
            this.state.categories = data.categories;
            
            if (cb) {
                cb();
            }
        });
    }
}