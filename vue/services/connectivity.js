export default {

    online() {
        if (navigator.onLine) {
            console.log('online...');
        }
    },

    offline() {
        if (navigator.offLine) {
            console.log('offline...');
        }
    }

}