import Teams from './pages/teams/index.vue'
import Dashboard from './pages/dashboard.vue'
import TeamDetails from './pages/teams/show.vue'
import ProjectDetails from './pages/projects/show.vue'
import Profile from './pages/account/profile.vue'
import UserLoggedTime from './pages/account/logged-time.vue'
import CompletedTask from './pages/tasks/completed-task.vue'

export default {
	'/': {
		component: Dashboard
	},

	'/teams': {
		component: Teams
	},

	'/teams/:id': {
		component: TeamDetails
	},
	
	'/projects/:id': {
		component: ProjectDetails,

		subRoutes: {
			'completed-task': {
				component: CompletedTask
			}
		}
	},

	'/user/:id': {
		component: Profile,

		subRoutes: {
			'logged-time': {
				component: UserLoggedTime
			}
		}
	}
}