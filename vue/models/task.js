import project from './project'

export default {
	project,
	id: null,
	title: '',
	description: '',
	start: '',
	end: '',
	done: false
}