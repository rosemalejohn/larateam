import team from './team'
import category from './category'

export default {
	id: 0,
    name: '',
    description: '',
    start: '0000-00-00',
    end: '0000-00-00',
    team,
    category
}