import config from './../config'

export default {
	id: null,
	name: '',
	email: '',
	photo: config.defaultPhoto,
	created_at: '0000-00-00',
	updated_at: '0000-00-00'
}