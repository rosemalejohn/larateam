export default {
    id: 0,
    name: '',
    website: '',
    email: '',
    industry: '',
    phone: '',
    fax: '',
    photo: '',
    line: '',
    state: '',
    zip: '',
    city: '',
    country: '',
    members: [],
    projects: []
}