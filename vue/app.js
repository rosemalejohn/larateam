import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import $ from 'jquery'
import App from './App.vue'
import routes from './routes'
import Ladda from 'ladda'

Vue.use(VueRouter);
Vue.use(VueResource);
Vue.http.options.root = '/api';
Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content')

new Vue(App);

var router = new VueRouter({
    // history: true,
    // hashbang: false,
    // root: '/',
    transitionOnLoad: true
});


Ladda.bind( 'button[type=submit]', { timeout: 3000 } );

router.map(routes).start(App, '#app');