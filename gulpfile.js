var elixir = require('laravel-elixir');

require('laravel-elixir-vueify');

elixir.config.sourcemaps = false;

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix
        .less('sb-admin-2.less')
        .browserify('app.js', null, 'vue')
        .copy('node_modules/sweetalert/dist/sweetalert.min.js', 'public/js/sweetalert.min.js')
        .copy('node_modules/sweetalert/dist/sweetalert.css', 'public/css/sweetalert.css')
        .copy('node_modules/dragula/dist/dragula.min.css', 'public/css/dragula.min.css')
        .copy('resources/assets/js/notify.js', 'public/js/notify.js');
});
