var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);

server.listen(3000, function() {
	console.log('Server booted...');
});

io.on('connection', function(socket) {
	console.log('Connection was made.');
});