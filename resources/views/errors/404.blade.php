@extends('layouts.guest')

@section('title', '404')

@section('content')
<div class="col-md-8">
	<h2>Aw snap! We can't find the page.</h2>
</div>
@stop