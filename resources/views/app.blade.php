
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>LaraTeam</title>

    <!-- Bootstrap Core CSS -->
    <link href="/lib/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/lib/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="/lib/startbootstrap-sb-admin-2/dist/css/timeline.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="/lib/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="/css/sweetalert.css" rel="stylesheet" type="text/css">

    <!-- Custom CSS -->
    <link href="/css/sb-admin-2.css" rel="stylesheet">

    <!-- Dragula CSS -->
    <link href="/css/dragula.min.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="http://github.hubspot.com/offline/themes/offline-theme-slide.css" type="text/css" />

    <link rel="stylesheet" href="/lib/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" type="text/css" />

    <link rel="stylesheet" href="/lib/animate.css/animate.min.css"  type="text/css" />

</head>

<body>

    <!-- <app></app> -->
    <div id="app"></div>

    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="/lib/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/lib/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="/lib/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="/lib/raphael/raphael.min.js"></script>
    <script src="/lib/morrisjs/morris.min.js"></script>
    <!-- <script src="../js/morris-data.js"></script> -->

    <!-- Custom Theme JavaScript -->
    <script src="/lib/startbootstrap-sb-admin-2/dist/js/sb-admin-2.js"></script>

    <script src="https://cdn.jsdelivr.net/vue.resource/0.9.1/vue-resource.min.js"></script>

    <script type="text/javascript" src="http://cdn.rawgit.com/HubSpot/offline/v0.7.13/offline.min.js"></script>

    <script type="text/javascript" src="http://cdn.rawgit.com/HubSpot/pace/v1.0.0/pace.min.js"></script>

    <!-- TinyMCE -->
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

    <!-- Moment -->
    <script src="/lib/moment/min/moment.min.js"></script>

    <!-- Datepicker -->
    <script src="/lib/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

    <!-- Notify.js -->
    <script src="/js/notify.js"></script>

    <!-- Sweetalert -->
    <script src="/js/sweetalert.min.js"></script>

    <!-- Vue compiled js files -->
    <script src="/js/app.js"></script>

    <!-- Socket.io -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.4.8/socket.io.js"></script>

</body>

</html>
