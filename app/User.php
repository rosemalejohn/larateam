<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'photo',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function time_logs()
    {
        return $this->hasManyThrough(TaskTimeLog::class, Task::class);
    }

    public function teams()
    {
        return $this->belongsToMany(Team::class, 'team_members');
    }
}
