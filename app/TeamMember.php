<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamMember extends Model
{

	public function team() {
		return $this->belongsTo(Team::class);
	}
    
	public function user() {
		return $this->belongsTo(User::class);
	}

	public function role() {
		return $this->belongsTo(Role::class);
	}

}
