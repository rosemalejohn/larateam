<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Procedure extends Model
{

	protected $fillable = ['procedure'];
    
	public function tasks() {
		return $this->belongsToMany(Task::class);
	}

}
