<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{

    protected $fillable = ['title', 'description', 'priority', 'start', 'end', 'done', 'project_id'];

    public function attachments()
    {
        return $this->belongsToMany(File::class);
    }

    public function comments()
    {
        return $this->hasMany(TaskComment::class);
    }

    public function estimate()
    {
        return $this->hasOne(TaskEstimate::class);
    }

    public function procedures()
    {
        return $this->belongsToMany(Procedure::class);
    }

    public function time_logs()
    {
        return $this->hasMany(TaskTimeLog::class);
    }

    public function task_parent()
    {
        return $this->hasOne(TaskParent::class);
    }

    public function task_child()
    {
        return $this->hasOne(TaskParent::class, 'parent_id');
    }

}
