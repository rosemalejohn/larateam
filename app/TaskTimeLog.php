<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskTimeLog extends Model
{

	protected $fillable = ['end'];
    
	public function task() {
		return $this->belongsTo(Task::class);
	}

	public function user() {
		return $this->belongsTo(User::class);
	}

}
