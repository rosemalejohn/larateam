<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskComment extends Model
{

	protected $fillable = ['comment'];

    public function task() {
    	return $this->belongsTo(Task::class);
    }
}
