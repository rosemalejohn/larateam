<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{

    protected $fillable = ['name', 'description', 'start', 'end', 'team_id', 'category_id'];

    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    public function notebooks()
    {
        return $this->hasMany(ProjectNotebook::class);
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

}
