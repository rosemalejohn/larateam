<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskEstimate extends Model
{

	protected $fillable = ['day', 'hour', 'min'];
    
	public function task() {
		return $this->belongsTo(Task::class);
	}

}
