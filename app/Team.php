<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{

	protected $fillable = [
		'name', 
		'website', 
		'email', 
		'industry', 
		'phone', 
		'fax', 
		'photo', 
		'line', 
		'state', 
		'zip_code', 
		'city', 
		'country'
	];

	public function members() {
		return $this->belongsToMany(User::class, 'team_members');
	}
    
	public function projects() {
		return $this->hasMany(Project::class);
	}

}
