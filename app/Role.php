<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{	

	protected $fillable = ['name'];

	public function team_members() {
		return $this->hasMany(TeamMember::class);
	}

	public function permission() {
		return $this->hasOne(RolePermission::class);
	}

}
