<?php

namespace App\Services;

use App\Team;

class TeamService {

    public function store($team) {
        $team = Team::create($team);
        auth()->user()->teams()->attach($team->id, ['role_id' => 1]);
        return $team;
    }

}