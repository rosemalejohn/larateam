<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Services\TeamService;
use App\Team;
use Illuminate\Http\Request;

class TeamController extends Controller
{

    public function index()
    {
        $teams = auth()->user()->teams()->with('projects', 'members')->get();
        return response()->json($teams);
    }

    public function store(Request $request, TeamService $team)
    {
        $team = $team->store($request->all());
        return response()->json($team->with('projects', 'members')->first());
    }

    public function show($id)
    {
        $team = Team::where('id', $id)->with('projects', 'members')->first();
        return response()->json($team);
    }

    public function delete(Team $team)
    {
        $team->delete();
        return response()->json(true);
    }

}
