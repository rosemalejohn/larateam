<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Project;
use Illuminate\Http\Request;
use App\Task;

class TaskController extends Controller
{

    public function store(Request $request)
    {
        $project = Project::findOrFail($request->project_id);
        $task = $project->tasks()->create($request->task);
        return response()->json($task);
    }

    public function delete(Task $task)
    {
        $task->delete();
        return response()->json(true)
    }   
}
