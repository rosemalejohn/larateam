<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Image;

class AuthController extends Controller
{
    public function getAuthenticatedUser()
    {
        return response()->json(auth()->user());
    }

    public function update(Request $request)
    {
        // if ($request->hasFile('photo')) {
        $request->photo = $this->uploadPhoto($request->photo);
        // }
        $user = auth()->user()->update($request->all());
        return response()->json($user);
    }

    public function uploadPhoto($photo)
    {
        $filename = public_path('/uploads/' . str_random(10) . '.jpg');
        Image::make($photo)->fit(500, 500)->save($filename);
        return $filename;
    }
}
