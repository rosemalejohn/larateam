<?php

Route::group(['middleware' => 'auth'], function () {

    Route::get('/', 'AppController@index');

    Route::group(['middleware' => 'api', 'prefix' => 'api', 'namespace' => 'API'], function () {

        Route::group(['prefix' => 'auth'], function () {

            Route::get('/', 'AuthController@getAuthenticatedUser');

            Route::put('/', 'AuthController@update');

        });

        Route::group(['prefix' => 'teams'], function () {

            Route::get('/', 'TeamController@index');

            Route::post('/', 'TeamController@store');

            Route::get('{team}', 'TeamController@show');

            Route::delete('{team}', 'TeamController@delete');

        });

        Route::group(['prefix' => 'projects'], function () {

            Route::post('/', 'ProjectController@store');

            Route::get('{project}', 'ProjectController@show');

            Route::put('{project}', 'ProjectController@update');

            Route::delete('{project}', 'ProjectController@delete');

        });

        Route::group(['prefix' => 'tasks'], function () {

            Route::post('/', 'TaskController@store');

            Route::delete('{task}', 'TaskController@delete');

        });

    });

});

Route::auth();
