<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{

	protected $fillable = ['name', 'path', 'type'];

    public function tasks() {
    	return $this->belongsToMany(Task::class);
    }

    public function comments() {
    	return $this->belongsToMany(TaskComment::class);
    }
}
