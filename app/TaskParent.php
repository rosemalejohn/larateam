<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskParent extends Model
{

	public function task() {
		return $this->belongsTo(Task::class);
	}

	public function parent() {
		return $this->belongsTo(Task::class, 'parent_id');
	}

}
